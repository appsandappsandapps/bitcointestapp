package com.example.swissborg.repositories

import com.example.swissborg.data.TickerItem
import com.example.swissborg.datasources.CurrencyLabelDatasource
import com.example.swissborg.datasources.TickerDatasource
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

class TickerRepositoryTest {

  val testDispatcher = StandardTestDispatcher()

  @MockK(relaxed = true)
  lateinit var labelsDatasource: CurrencyLabelDatasource

  @MockK(relaxed = true)
  lateinit var tickerDatasource: TickerDatasource

  @Before fun setUp() {
    MockKAnnotations.init(this)
    Dispatchers.setMain(testDispatcher)
  }

  @After fun tearDown() {
    Dispatchers.resetMain()
  }

  @Test
  fun `ticker repo returns ticker item with correct price`() = runTest {
    coEvery { tickerDatasource.getTickers() } returns listOf(TickerItem("bBTCUSD", 1F, 2F))
    coEvery { labelsDatasource.getLabels() } returns mapOf(Pair("BTC", "Bitcoin"))

    val repo = TickerRepository(tickerDatasource, labelsDatasource, CoroutineScope(testDispatcher))
    launch { repo.getTickerItems() }
    advanceUntilIdle()

    assertEquals(2F, repo.tickerItems.first()[0].price)
  }

  @Test
  fun `ticker repo returns ticker item with correct change`() = runTest {
    coEvery { tickerDatasource.getTickers() } returns listOf(TickerItem("bBTCUSD", 1F, 2F))
    coEvery { labelsDatasource.getLabels() } returns mapOf(Pair("BTC", "Bitcoin"))

    val repo = TickerRepository(tickerDatasource, labelsDatasource, CoroutineScope(testDispatcher))
    launch { repo.getTickerItems() }
    advanceUntilIdle()

    assertEquals(1F, repo.tickerItems.first()[0].change)
  }

  @Test
  fun `ticker repo returns ticker item with correct verbose label`() = runTest {
    coEvery { tickerDatasource.getTickers() } returns listOf(TickerItem("bBTCUSD"))
    coEvery { labelsDatasource.getLabels() } returns mapOf(Pair("BTC", "Bitcoin"))

    val repo = TickerRepository(tickerDatasource, labelsDatasource, CoroutineScope(testDispatcher))
    launch { repo.getTickerItems() }
    advanceUntilIdle()

    assertEquals("Bitcoin", repo.tickerItems.first()[0].label)
  }

}