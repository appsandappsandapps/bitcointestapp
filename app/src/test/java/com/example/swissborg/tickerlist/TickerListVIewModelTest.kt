package com.example.swissborg.tickerlist

import androidx.activity.compose.rememberLauncherForActivityResult
import com.example.swissborg.repositories.TickerRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

class TickerListVIewModelTest {

  val testDispatcher = StandardTestDispatcher()

  @MockK(relaxed = true)
  lateinit var tickerRepository: TickerRepository

  @Before
  fun setUp() {
    MockKAnnotations.init(this)
    Dispatchers.setMain(testDispatcher)
  }

  @After
  fun tearDown() {
    Dispatchers.resetMain()
  }

  @Test
  fun `test view model calls repo on init`() = runTest {

    TickerListViewModel(tickerRepository, testDispatcher)
    advanceUntilIdle()

    coVerify(exactly = 1) { tickerRepository.startFetchingTicker() }
  }

}