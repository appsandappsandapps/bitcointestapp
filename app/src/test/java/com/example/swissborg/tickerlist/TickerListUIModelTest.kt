package com.example.swissborg.tickerlist

import com.example.swissborg.data.TickerItem
import com.example.swissborg.repositories.TickerRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

class TickerListUIModelTest {

  val testDispatcher = StandardTestDispatcher()

  @MockK(relaxed = true)
  lateinit var tickerRepository: TickerRepository

  @Before
  fun setUp() {
    MockKAnnotations.init(this)
    Dispatchers.setMain(testDispatcher)
  }

  @After
  fun tearDown() {
    Dispatchers.resetMain()
  }

  @Test
  fun `test filter by symbol correct`() {
    val filtered = TickerListUIState(
      tickerItems = listOf(TickerItem("BTC", label = "Bitcoin"), TickerItem("ETH", label = "Ethereum")),
      filterText = "BTC"
    ).possiblyFiltered

    assertEquals(1, filtered.size)
    assertEquals("BTC", filtered[0].symbol)
  }

  @Test
  fun `test filter by label correct`() {
    val filtered = TickerListUIState(
      tickerItems = listOf(TickerItem("BTC", label = "Bitcoin"), TickerItem("ETH", label = "Ethereum")),
      filterText = "Bitc"
    ).possiblyFiltered

    assertEquals(1, filtered.size)
    assertEquals("Bitcoin", filtered[0].label)
  }

}