package com.example.swissborg.data

import com.example.swissborg.repositories.TickerRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.Test

import org.junit.Assert.*

class TIckerItemTest {

  val testDispatcher = StandardTestDispatcher()

  @Test
  fun `test TickerItem parses coins symbol correctly USD`() {

    val coinSymbol = TickerItem("bBTCUSD").coinSymbol()

    assertEquals("BTC", coinSymbol)
  }

  @Test
  fun `test TickerItem parses coins symbol correctly EUR`() {

    val coinSymbol = TickerItem("bBTCEUR").coinSymbol()

    assertEquals("BTC", coinSymbol)
  }

  @Test
  fun `test TickerItem parses four letter coins symbol correctly EUR`() {

    val coinSymbol = TickerItem("bBTCXEUR").coinSymbol()

    assertEquals("BTCX", coinSymbol)
  }

  @Test
  fun `test TickerItem parses coins symbol with colon correctly EUR`() {

    val coinSymbol = TickerItem("bBTCX:EUR").coinSymbol()

    assertEquals("BTCX", coinSymbol)
  }
}