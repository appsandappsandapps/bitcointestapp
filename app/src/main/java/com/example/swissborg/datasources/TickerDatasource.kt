package com.example.swissborg.datasources

import android.util.Log
import com.example.swissborg.GenericRESTSource
import com.example.swissborg.data.TickerItem
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.float
import kotlinx.serialization.json.jsonPrimitive

interface TickerDatasource {
  suspend fun getTickers(): List<TickerItem>
}

class TickerDatasourceImp(
  private val httpDatasource: GenericRESTSource = GenericRESTSource(),
): TickerDatasource {

  companion object {
    // TODO: Put in config file
    val url = "https://api-pub.bitfinex.com/v2/tickers?symbols=tBTCUSD,tETHUSD,tCHSB:USD,tLTCUSD,tXRPUSD,tDSHUSD, tRRTUSD,tEOSUSD,tSANUSD,tDATUSD,tSNTUSD,tDOGE:USD,tLUNA:USD,tMATIC:USD,tNEXO:USD,tOCEAN:USD,tBEST:USD, tAAVE:USD,tPLUUSD,tFILUSD"
  }

  override suspend fun getTickers(): List<TickerItem> {
    val items: List<JsonArray> = httpDatasource.get(url)
    return items.map {
      val symbol = it[0].jsonPrimitive.content
      val change = it[6].jsonPrimitive.float * 100
      val price = it[7].jsonPrimitive.float
      TickerItem(symbol, change, price)
    }
  }

}

