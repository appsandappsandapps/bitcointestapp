package com.example.swissborg.datasources

import android.util.Log
import com.example.swissborg.GenericRESTSource
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.jsonArray
import kotlinx.serialization.json.jsonPrimitive

interface CurrencyLabelDatasource {
  suspend fun getLabels(): Map<String, String>
}

class CurrencyLabelDatasourceImp(
  private val httpDatasource: GenericRESTSource = GenericRESTSource(),
): CurrencyLabelDatasource {

  companion object {
    // TODO: Put in config file
    val url = "https://api-pub.bitfinex.com/v2/conf/pub:map:currency:label"
  }

  override suspend fun getLabels(): Map<String, String> {
    val items: List<List<JsonArray>> = httpDatasource.get(url)
    return items[0].fold(mutableMapOf<String, String>()) { acc, labelArray ->
      acc.put(
        labelArray[0].jsonPrimitive.content,
        labelArray[1].jsonPrimitive.content,
      )
      acc
    }
  }

}

