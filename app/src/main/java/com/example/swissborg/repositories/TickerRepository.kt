package com.example.swissborg.repositories

import android.util.Log
import com.example.swissborg.data.TickerItem
import com.example.swissborg.datasources.CurrencyLabelDatasource
import com.example.swissborg.datasources.TickerDatasource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import java.util.*

/**
 * - Fetches the ticker every 5 seconds
 * - Indicates if there's been a fetching problem
 */
// TODO: Use a `UseCase` that combines the datasources/repos
class TickerRepository(
  private val tickerDatasource: TickerDatasource,
  private val currencyLabelDatasource: CurrencyLabelDatasource,
  private val coroutineScope: CoroutineScope = CoroutineScope(SupervisorJob() + Dispatchers.IO),
) {

  val tickerItems = MutableStateFlow<List<TickerItem>>(listOf())
  val fetchingActive = MutableStateFlow<Boolean>(true)
  private var timer: Timer? = null

  // TODO: Put time in config / service locator
  // TODO: Ability to stop the timer
  suspend fun startFetchingTicker(period: Long = 5000L) {
    kotlin.concurrent.timer(period = period) {
      coroutineScope.launch {
        try {
          getTickerItems()
          fetchingActive.value = true
        } catch(e: Exception) {
          fetchingActive.value = false
        }
      }
    }
  }

  suspend fun getTickerItems() {
    val labels = currencyLabelDatasource.getLabels()
    val tickers = tickerDatasource.getTickers()
    tickers.forEach { ticker ->
      val label = labels.get(ticker.coinSymbol())
      label?.let { ticker.label = it }
    }
    tickerItems.value = tickers
  }

}