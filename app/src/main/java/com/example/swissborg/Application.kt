package com.example.swissborg

import android.app.Application

class Application : Application() {
  override fun onCreate() {
    super.onCreate()
    ServiceLocator.init()
  }
}