package com.example.swissborg.tickerlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.swissborg.ServiceLocator
import com.example.swissborg.repositories.TickerRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * - Starts fetching the data
 * - Listens for a problem in connectivity
 */
class TickerListViewModel(
  private val tickerRepository: TickerRepository = ServiceLocator.tickerRepository,
  private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO,
): ViewModel() {

  val uiModel = TickerListUIModel(this)

  init {
    viewModelScope.launch(ioDispatcher) {
      tickerRepository.tickerItems.collect {
        uiModel.setTickerListItems(it)
      }
    }
    viewModelScope.launch(ioDispatcher) {
      tickerRepository.fetchingActive.collect {
        uiModel.setFetchingActive(it)
      }
    }
    viewModelScope.launch(ioDispatcher) {
      tickerRepository.startFetchingTicker()
    }
  }

}