package com.example.swissborg.tickerlist

import com.example.swissborg.data.TickerItem
import kotlinx.coroutines.flow.MutableStateFlow

data class TickerListUIState(
  val tickerItems: List<TickerItem> = listOf(),
  val filterText: String = "",
  val fetchingActive: Boolean = false,
) {

  /**
   * If the filterText is there, then filter the list
   * otherwise give the unfiltered list
   */
  val possiblyFiltered: List<TickerItem>
    get() {
      if (filterText.isBlank()) {
        return tickerItems
      } else {
        return tickerItems.filter {
          val inSymbol = it.symbol.lowercase().contains(filterText, true)
          val inLabel = it.label.lowercase().contains(filterText, true)
          inSymbol || inLabel
        }
      }
    }

}

class TickerListUIEvents(
  val tickerListViewModel: TickerListViewModel,
  val uiState: MutableStateFlow<TickerListUIState>,
) {

  fun setFilterText(filterText: String) {
    uiState.value = uiState.value.copy(
      filterText = filterText
    )
  }

}

class TickerListUIModel(
  val tickerListViewModel: TickerListViewModel
) {

  val uiState = MutableStateFlow(TickerListUIState())
  val uiEvents = TickerListUIEvents(tickerListViewModel, uiState)

  fun setTickerListItems(items: List<TickerItem>) {
    uiState.value = uiState.value.copy(
      tickerItems = items
    )
  }

  fun setFetchingActive(error: Boolean) {
    uiState.value = uiState.value.copy(
      fetchingActive = error
    )
  }

}