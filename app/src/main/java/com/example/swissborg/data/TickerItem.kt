package com.example.swissborg.data

// TODO: Separate data objects into ui and non domain
data class TickerItem(
  val symbol: String = "",
  val change: Float = 0F,
  val price: Float = 0F,
  var label: String = symbol, // is reset with a proper name
) {

  // TODO: Add more currencies
  // and get them from an array
  fun coinSymbol(): String =
    symbol
      .replace("USD$".toRegex(), "")
      .replace("EUR$".toRegex(), "")
      .replace(":", "")
      .substring(startIndex = 1) // codes start with `t`
}
