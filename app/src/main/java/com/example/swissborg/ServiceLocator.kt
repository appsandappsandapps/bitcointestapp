package com.example.swissborg

import com.example.swissborg.datasources.CurrencyLabelDatasourceImp
import com.example.swissborg.datasources.TickerDatasourceImp
import com.example.swissborg.repositories.TickerRepository

object ServiceLocator {

  lateinit var tickerRepository: TickerRepository private set

  fun init() {
    val tickerDatasource = TickerDatasourceImp()
    val currencyLabelDatasource = CurrencyLabelDatasourceImp()
    tickerRepository = TickerRepository(
      tickerDatasource,
      currencyLabelDatasource,
    )
  }
}
