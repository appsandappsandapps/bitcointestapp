package com.example.swissborg.ui

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.swissborg.data.TickerItem
import com.example.swissborg.tickerlist.TickerListUIState
import com.example.swissborg.tickerlist.TickerListViewModel

// TODO: A loading screen/spinner
// TOOD: An error page
// TODO: A view when filtering has no results
// TOOD: Does the material3 surface look worse than 2?
@Composable
fun TickerList() {
  val viewModel = viewModel { TickerListViewModel() }
  val uiModel = viewModel.uiModel
  val uiState by uiModel.uiState.collectAsState()
  val uiEvents = uiModel.uiEvents

  Column {
    ActiveIndicator(uiState.fetchingActive)
    TextField(
      uiState.filterText,
      { uiEvents.setFilterText(it) },
      Modifier.fillMaxWidth(),
      placeholder = { Text("Please enter filter text") }
    )
    LazyColumn {
      items(uiState.possiblyFiltered) {
        Surface(
          Modifier
            .padding(10.dp),
          shadowElevation = 2.dp,
          tonalElevation = 0.dp,
          border = BorderStroke(1.dp, Color.LightGray),
          shape = RoundedCornerShape(6.dp),
        ) {
          Row(
            Modifier
              .padding(8.dp)
          ) {
            NameAndSymbol(it)
            Spacer(Modifier.weight(1F))
            PriceAndChange(it)
          }
        }
      }
    }
  }

}

@Composable
private fun NameAndSymbol(it: TickerItem) {
  Column {
    Text(
      it.label,
      fontSize = 20.sp,
      fontWeight = FontWeight.Bold,
    )
    Text(
      it.coinSymbol(),
      color = Color.Gray,
    )
  }
}

// TODO: Get the currency from the repo
@Composable
private fun PriceAndChange(it: TickerItem) {
  Column(
    horizontalAlignment = Alignment.End,
  ) {
    Text(
      "$${it.price}",
      fontSize = 20.sp,
      fontWeight = FontWeight.Bold,
    )
    Text(
      "${it.change}%",
      color = Color.Gray,
    )
  }
}

/**
 * Shows a red bar when we aren't fetching
 * Or a green if we are
 */
@Composable
private fun ActiveIndicator(fetchingActive: Boolean) {
  Text(
    "Fetching every 5 secs: " + (if (fetchingActive) "active" else "inactive"),
    Modifier
      .fillMaxWidth()
      .background(
        if (fetchingActive) {
          Color.Green.copy(alpha = 0.5f)
        } else {
          Color.Red.copy(alpha = 0.5f)
        }
      )
      .padding(top = 4.dp, bottom = 4.dp),
    fontSize = 12.sp,
    textAlign = TextAlign.Center,
  )
}
